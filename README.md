# Snicker example repository

This repository is a basic example for using Snicker/Snitch-CI in GitLab, and perform automated graphical testing of your application.

Our sample app is a basic Qt window (from https://wiki.qt.io/Qt_for_Beginners), which was tested by Snicker with following steps:

1. On local workstation (eg. Ubuntu Linux):
  - __package app__ [with dependencies embeded]: 
      ```bash
      cd Snicker-HelloWorld
      qmake
      make
      ``` 
  - __install Snicker__ somewhere: `git clone https://gitlab.com/IRSN/Snicker`
  - [install Snicker dependencies]: 'docker', 'pkill', 'xtightvncviewer'
  - create Snicker test case: `sh Snicker/snicker.sh`
    -  [at first launch] it will setup the __'irsn/snicker' container__
    - then it will launch its __VNC desktop window__:
      - with an 'xterm' window
      - with a 'Snitch' window already recording your actions (minimized)
      - note that the host working dir is in '/WORKDIR' (where you should find your application to test)
    - __record your test__, following Snitch guidelines (https://gitlab.com/irsn/snitch-ci)
    - carefully select the __final screenshot__ for Snitch OCR
    - then __save your 'record.json'__ in '/WORKDIR' where it will persist over docker termination
    - __close xterm window__, which will also close VNC session and terminate docker
  - __replay your test case__: `sh Snicker/snicker.sh -f record.json -v` (you should see your test case playing, non interactive). It should return a '0' exit status if test passed
  - [if something goes wrong]: you can edit your test case (like adding delays between actions) using 'Snitch' directly (`pip3 install snitch-ci`) and loading 'record.json' file. You can edit actions and insert Delays (often needed).
2. On your GitLab repository, you have to push/append:
  - previous 'record.json' file
  - '.gitlab-ci.yml':
    ```yml
    image: ubuntu:18.04
    
    stages:
      - build
      - test
      # UI testing is in next stage:
      - snitch
    
    before_script:
        - apt-get update
        - apt-get install -y qt5-default libqt5widgets5
    
    build:
      stage: build
      script:
        - apt-get install -y build-essential
        - qmake
        - make
      artifacts:
        paths:
        - Snicker-HelloWorld
        expire_in: 1 week
    
    test:
      stage: test
      dependencies:
        - build
      script:
        - echo "do some unit tests here"

    after_script:
      - ls -la *
      - more *.log


    # All previous stages are standard. Here starts the part for UI testing:
    snitch:
      stage: snitch
      image:
        # Dedicated Ubuntu 18.04 image with Snicker & dependencies
        name: irsn/snicker
        # But disable its standard ENTRYPOINT
        entrypoint: [""]
      dependencies:
        - build
      script:
        # Setup working directory
        - rm -rf /WORKDIR && ln -s /builds/irsn/snicker-helloworld /WORKDIR
        # Launch previously disabled ENTRYPOINT. Will return an 'exit 0' if test passed.
        - /bin/bash /snicker/run_snitch.sh -f record.json -d
      artifacts:
        paths:
        - ./log/
        - ./*.json
        - ./*.png
        expire_in: 1 month
        when: always
    ```
    
